#include <iostream>
#include <cmath>

#include "bitmap/bitmap_image.hpp"

int f(int i) {
	return 150 - 100 * std::sin((i-150)*9.0 / 300.0);
}

int main() {
	bitmap_image img(300, 300); // set image size
	img.set_all_channels(255, 255, 255); // set white background

	image_drawer draw(img);
	draw.pen_width(1);

	// draw axes
	draw.pen_color(0, 0, 255);
	draw.line_segment(0, 150, 299, 150);
	draw.line_segment(150, 0, 150, 299);

	// draw sin(x)
	draw.pen_color(0, 0, 0);
	int prev_y = f(0);
	for (int i=1; i<300; i++) {
		int y = f(i);
		draw.line_segment(i-1, prev_y, i, y);
		prev_y = y;
	}

	img.save_image("example.bmp");
}
